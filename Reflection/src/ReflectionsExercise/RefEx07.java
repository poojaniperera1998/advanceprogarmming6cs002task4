package ReflectionsExercise;

public class RefEx07 {

	  public static void main(String[] args) throws Exception {
	    ReflectionSimple simpleRef = new ReflectionSimple();
	    java.lang.reflect.Field[] f1 = simpleRef.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", f1.length);

	    for (java.lang.reflect.Field f02 : f1) {
	      f02.setAccessible(true);
	      System.out.printf("field name=%s type=%s value=%d\n", f02.getName(),
	          f02.getType(), f02.getLong(simpleRef));
	    }
	  }
}
