package ReflectionsExercise;

public class RefEx03 {
	public static void main(String[] args) {
		ReflectionSimple simpleRef = new ReflectionSimple();
		System.out.println("class = " + simpleRef.getClass());
		System.out.println("class name = " + simpleRef.getClass().getName());
	}
}
