package ReflectionsExercise;

import java.text.DateFormat.Field;

public class RefEx08 {
	public static void main(String[] args) throws Exception {
	    ReflectionSimple simpleRef = new ReflectionSimple();
	    java.lang.reflect.Field[] f1 = simpleRef.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", f1.length);
	    for (java.lang.reflect.Field f2 : f1) {
	      f2.setAccessible(true);
	      Long x = f2.getLong(simpleRef);
	      x++;
	      f2.setLong(simpleRef, x);
	      System.out.printf("field name=%s type=%s value=%d\n", f2.getName(),
	          f2.getType(), f2.getLong(simpleRef));
	    }
	  }
}
