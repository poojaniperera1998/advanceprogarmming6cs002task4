package ReflectionsExercise;

import java.lang.reflect.Method;

public class RefEx09 {
	public static void main(String[] args) throws Exception {
	    ReflectionSimple simpleRef = new ReflectionSimple();
	    Method[] m1 = simpleRef.getClass().getMethods();
	    System.out.printf("There are %d methods\n", m1.length);

	    for (Method m2 : m1) {
	      System.out.printf("method name=%s type=%s parameters = ", m2.getName(),
	          m2.getReturnType());
	      Class[] types = m2.getParameterTypes();
	      for (Class c : types) {
	        System.out.print(c.getName() + " ");
	      }
	      System.out.println();
	    }
	  }
}
