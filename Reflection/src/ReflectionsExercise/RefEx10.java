package ReflectionsExercise;

import java.lang.reflect.Method;

public class RefEx10 {
	public static void main(String[] args) throws Exception {
	    ReflectionSimple simpleRef = new ReflectionSimple();
	    Method method = simpleRef.getClass().getDeclaredMethod("setOne",
	    		long.class);
	    method.setAccessible(true);
	    method.invoke(simpleRef, 59);
	    System.out.println(simpleRef);
	  }
}
