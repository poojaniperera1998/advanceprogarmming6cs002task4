package ReflectionsExercise;

public class RefEx05 {
	 public static void main(String[] args) throws Exception {
		    ReflectionSimple simpleRef = new ReflectionSimple();
		    java.lang.reflect.Field[] f1 = simpleRef.getClass().getDeclaredFields();
		    System.out.printf("There are %d fields\n", f1.length);

		    for (java.lang.reflect.Field f2 : f1) {
		      System.out.printf("field name=%s type=%s value=%d\n", f2.getName(),
		          f2.getType(), f2.getLong(simpleRef));
		    }
		  }
}
