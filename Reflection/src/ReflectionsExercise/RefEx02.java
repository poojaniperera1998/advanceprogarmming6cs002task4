package ReflectionsExercise;

public class RefEx02 {
	 public static void main(String[] args) {
		    ReflectionSimple simpleRef = new ReflectionSimple();
		    simpleRef.squareOne();
		    // simple.squareK(); 
		    long k = simpleRef.inputOne;
		    // long k = simple.k; 
		    System.out.println("Result=" + simpleRef);
		  }
}
