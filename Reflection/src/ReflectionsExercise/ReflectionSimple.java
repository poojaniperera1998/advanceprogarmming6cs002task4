package ReflectionsExercise;
public class ReflectionSimple {

		  public long inputOne = 40;
		  private long inputTwo = 50;
		  public long inputThree = 60;

		  public ReflectionSimple() {
		  }
		  public ReflectionSimple(long k, long m,long n) {
		  this.inputOne = k;
		  this.inputTwo = m;
		  this.inputThree = n;
		  }

		  public void squareOne() {
		    this.inputOne *= this.inputOne;
		  }

		  private void squareTwo() {
		    this.inputTwo *= this.inputTwo;
		  }
		  public void squareThree() {
			    this.inputThree *= this.inputThree;
		  }
		 


		public long getOne() {
			return inputOne;
		}

		public void setOne(long k) {
			this.inputOne = k;
		}

		public long getTwo() {
			return inputTwo;
		}

		private void setTwo(long m) {
			this.inputTwo = m;
		}

		public long getThree() {
			return inputThree;
		}

		public void setThree(long n) {
			this.inputThree = n;
		}
		
		public String toString() {
			return String.format("(inpuOne:%d, inputTwo:%d, inputThree:%d)", inputOne, 
					inputTwo, inputThree);
		}

}

